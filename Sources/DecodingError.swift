import Foundation

public enum DecodingError: Error {
  case missingProperty(String)
  case typeMismatch(expected: String, actual: String)
}