import Foundation

extension Dictionary where Key == String, Value == Any {

  /// Gets the specified property from the json object as T
  public func getRequired<T>(property: String) throws -> T {
    guard let value: T = self.getOptional(property: property) else {
      throw DecodingError.missingProperty(property)
    }

    return value
  }

  /// Gets the specified property from the json object as T or nil if not present.
  public func getOptional<T>(property: String) throws -> T? {
    guard let node: Any = self[property] else { return nil }
    guard let value = node as? T else {
      let expectedType = String(describing: type(of: T.self))
      let actualType = String(describing: type(of: node))
      throw DecodingError.typeMismatch(expected: expectedType, actual: actualType)
    }

    return value
  }

  /// Extracts the specified node as a JsonObject and returns a new instance of T
  public func decodeJsonObject<T:JsonDecodable>(_ name: String) throws -> T {
    let node: JsonObject = try self.getRequired(property: name)
    return try node.decoded()
  }

  /// Returns self decoded as a new instance of T
  public func decoded<T:JsonDecodable>() throws -> T {
    return try T(json: self)
  }

  /// Extracts the specified node as an array of JsonObjects and maps each one to a new instance of T
  public func decodeJsonArray<T:JsonDecodable>(_ name: String) throws -> [T] {
    let nodes: [JsonObject] = try self.getRequired(property: name)
    return try T.array(json: nodes)
  }
}