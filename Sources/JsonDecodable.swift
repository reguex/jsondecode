import Foundation

public typealias JsonObject = [String: Any]

public protocol JsonDecodable {
  init(json: JsonObject) throws
}

extension JsonDecodable {
  public static func array(json: [JsonObject]) throws -> [Self] {
    var result = [Self]()
    for node in json {
      try result.append(Self.init(json: node))
    }
    return result
  }
}