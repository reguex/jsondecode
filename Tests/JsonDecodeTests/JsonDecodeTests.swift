import XCTest
@testable import JsonDecode

class JsonDecodeTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(JsonDecode().text, "Hello, World!")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}
