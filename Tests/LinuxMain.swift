import XCTest
@testable import JsonDecodeTests

XCTMain([
    testCase(JsonDecodeTests.allTests),
])
